/*
 * Copyright 2022 James Davidson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

function btnStart_onClick()
{
    // Initialise the loop variables
    var parElement;
    var i;

    // Define the array parameters
    const arrayLength = 5;
    const maxValue = 49;

    /*
     * Use this StackOverflow solution to generate unique random numbers:
     *
     * https://stackoverflow.com/questions/2380019/generate-unique-random-numbers-between-1-and-100/2380113#2380113
     */

    var uniqueRandomNumberArray = [];
    while(uniqueRandomNumberArray.length <= arrayLength)
        {
            // Generate a number between 1 and maxValue
            var randomNumber = Math.floor(Math.random() * maxValue) + 1;

            /* 
             * Don't push randomNumber if it's equal to other numbers in the
             * array
             */

            if(uniqueRandomNumberArray.indexOf(randomNumber) === -1)
                uniqueRandomNumberArray.push(randomNumber);
        }

    // Generate .textContent for all parNumber elements
    for (i = 0; i <= arrayLength; i++)
    {
        number = uniqueRandomNumberArray[i];

        parElement = 'parNumber' + i;
        var paragraph = document.getElementById(parElement)

        /*
         * Reset background colour to ensure the if statements are respected
         * after clicking Start again
         */

        paragraph.style.background = "white";

        /*
         * Add text to the paragraph
         * e.g. "Number 3: 14"
         */

        paragraph.textContent = 'Number ' + (i + 1) +
            ': ' + number;

         /*
          * Test number in ascending order, to ensure the correct background
          * colour is chosen
          */

        if (number <= 9) {
            paragraph.style.background = "grey";
        } else if (number <= 19) {
            paragraph.style.background = "blue";
        } else if (number <= 29) {
            paragraph.style.background = "pink";
        } else if (number <= 39) {
            paragraph.style.background = "green";
        } else if (number <= 49) {
            paragraph.style.background = "yellow";
        }
    }
}
